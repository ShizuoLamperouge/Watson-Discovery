import { Component, OnInit,Input } from '@angular/core';
import {Response} from '../response';

@Component({
  selector: 'app-concepts',
  templateUrl: './concepts.component.html',
  styleUrls: ['./concepts.component.css']
})
export class ConceptsComponent implements OnInit {

  @Input() response:Response;
  constructor() { }

  ngOnInit() {
  }

}
