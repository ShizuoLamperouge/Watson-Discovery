export class Response {
    title:string;
    concepts:Array<string>;
    average:number;
    results:Array<number>;
}
