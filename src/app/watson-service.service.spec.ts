import { TestBed, inject } from '@angular/core/testing';

import { WatsonServiceService } from './watson-service.service';

describe('WatsonServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [WatsonServiceService]
    });
  });

  it('should be created', inject([WatsonServiceService], (service: WatsonServiceService) => {
    expect(service).toBeTruthy();
  }));
});
