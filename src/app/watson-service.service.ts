import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { HttpErrorResponse } from '@angular/common/http/src/response';
import 'rxjs/add/operator/retry';
import 'rxjs/add/operator/map';
import { forEach } from '@angular/router/src/utils/collection';
import {Response} from './response';

@Injectable()
export class WatsonServiceService {

//    username: "dbdb08c2-9aa9-4ea5-b757-854732ae825e",
//    password: "VsaoWgJ1UiPk",

  constructor(private http: HttpClient) { }

  searchTest(){
    let headers = new HttpHeaders();
    headers = headers.append("Authorization", "Basic " + btoa("dbdb08c2-9aa9-4ea5-b757-854732ae825e:VsaoWgJ1UiPk"));
    headers = headers.append("Content-Type", "application/x-www-form-urlencoded");
    this.http.get(
      'https://gateway.watsonplatform.net/discovery/api/v1/environments/system/collections/news-en/query?version=2017-11-07&aggregation=%5Bterm%28enriched_text.concepts.text%2Ccount%3A10%29%2Ctimeslice%28publication_date%2C1month%2Canomaly%3Atrue%29%2Caverage%28enriched_text.entities.sentiment.score%29%2Cterm%28enriched_text.entities.sentiment.label%2Ccount%3A10%29%5D&deduplicate=false&highlight=true&return=title%2Curl%2Clanguage%2Cenriched_text.sentiment.document&passages=true&passages.count=5&natural_language_query=bitcoin',
      {headers:headers}
    )
    .retry(3)
    .subscribe(
      data =>  console.log(JSON.stringify(data, null, 2))
    );
  }

  searchMockedTest(){
    return this.http.get('assets/response.json')
    .retry(3)
    .map((res: Response) => this.hydrate(res))
  }
  
  hydrate(data:any):Response{
    
    let concepts:Array<string>=new Array();
    for(let a of data.aggregations[0].results){
      concepts.push(a.key);
    }
    
    let average:number = data.aggregations[2].value;

    let results:Array<number>=new Array();
    results.push(Math.round((data.aggregations[3].results[0].matching_results/(data.aggregations[3].results[0].matching_results+data.aggregations[3].results[1].matching_results+data.aggregations[3].results[2].matching_results))*10000)/100)
    results.push(Math.round((data.aggregations[3].results[1].matching_results/(data.aggregations[3].results[0].matching_results+data.aggregations[3].results[1].matching_results+data.aggregations[3].results[2].matching_results))*10000)/100)
    results.push(Math.round((data.aggregations[3].results[2].matching_results/(data.aggregations[3].results[0].matching_results+data.aggregations[3].results[1].matching_results+data.aggregations[3].results[2].matching_results))*10000)/100)
    let response = new Response();
    response.concepts=concepts;
    response.average=(Math.round(average*1000))/100;
    response.results=results;
    response.title="bitcoin"
    return response;
  }
  
}