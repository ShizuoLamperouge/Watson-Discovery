import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms/';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { NavigationBarComponent } from './navigation-bar/navigation-bar.component';
import { FooterComponent } from './footer/footer.component';
import { MainSectionComponent } from './main-section/main-section.component';
import { SearchComponent } from './search/search.component';
import { ScoreAverageComponent } from './score-average/score-average.component';
import { SentimentsComponent } from './sentiments/sentiments.component';
import { ConceptsComponent } from './concepts/concepts.component';
import { TimesliceComponent } from './timeslice/timeslice.component';
import { ArticlesComponent } from './articles/articles.component';
import { OverviewComponent } from './overview/overview.component';
import { WatsonServiceService } from './watson-service.service';
import { AuthService } from './auth.service';
import { HttpClientModule } from '@angular/common/http';


@NgModule({
  declarations: [
    AppComponent,
    NavigationBarComponent,
    FooterComponent,
    MainSectionComponent,
    SearchComponent,
    ScoreAverageComponent,
    SentimentsComponent,
    ConceptsComponent,
    TimesliceComponent,
    ArticlesComponent,
    OverviewComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [
    WatsonServiceService,
    AuthService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
