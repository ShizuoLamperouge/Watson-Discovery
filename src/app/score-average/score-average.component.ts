import { Component, OnInit, Input } from '@angular/core';
import {Response} from '../response';

@Component({
  selector: 'app-score-average',
  templateUrl: './score-average.component.html',
  styleUrls: ['./score-average.component.css']
})
export class ScoreAverageComponent implements OnInit {

  @Input() response:Response;
  constructor() { }

  ngOnInit() {
  }

}
