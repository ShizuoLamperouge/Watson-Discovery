import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ScoreAverageComponent } from './score-average.component';

describe('ScoreAverageComponent', () => {
  let component: ScoreAverageComponent;
  let fixture: ComponentFixture<ScoreAverageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ScoreAverageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScoreAverageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
