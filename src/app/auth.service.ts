import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { HttpErrorResponse } from '@angular/common/http/src/response';
import 'rxjs/add/operator/retry';

@Injectable()
export class AuthService {

  token:string;

  constructor(private http: HttpClient) { }

  handshake() {
    let headers = new HttpHeaders();
    headers = headers.append("Authorization", "Basic " + btoa("dbdb08c2-9aa9-4ea5-b757-854732ae825e:VsaoWgJ1UiPk"));
    headers = headers.append("Content-Type", "application/x-www-form-urlencoded");
    this.http.get(
      'https://gateway.watsonplatform.net/authorization/api/v1/token?url=https://gateway.watsonplatform.net/discovery/api',
      {headers:headers}
    )
    //.retry(3)
    .subscribe(
      data =>  console.log(JSON.stringify(data, null, 2))
    );
  }
}
