import { Component, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent {

  keyword:string="";

  @Output()
  search:EventEmitter<string> = new EventEmitter();

  constructor() { }

  searchKeyword(){
    this.search.emit(this.keyword);
    this.keyword="";
  }
}
