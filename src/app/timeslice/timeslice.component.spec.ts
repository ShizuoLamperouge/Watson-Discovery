import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TimesliceComponent } from './timeslice.component';

describe('TimesliceComponent', () => {
  let component: TimesliceComponent;
  let fixture: ComponentFixture<TimesliceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TimesliceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TimesliceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
