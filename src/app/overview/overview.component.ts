import { Component, OnInit, Input } from '@angular/core';
import { Response } from '../response';

@Component({
  selector: 'app-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.css']
})
export class OverviewComponent implements OnInit {

  @Input()
  response:Response;

  constructor() { 
    this.response=new Response;
  }

  ngOnInit() {
  }
}
