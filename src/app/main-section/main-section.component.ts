import { Component, OnInit } from '@angular/core';
import {WatsonServiceService} from '../watson-service.service';
import { AuthService } from '../auth.service';
import { Input } from '@angular/core/src/metadata/directives';
import {Response} from '../response';


@Component({
  selector: 'app-main-section',
  templateUrl: './main-section.component.html',
  styleUrls: ['./main-section.component.css']
})
export class MainSectionComponent implements OnInit {

  res:Response;
  constructor(private watsonService:WatsonServiceService,private authService:AuthService) {
    this.res=new Response();
   }

  ngOnInit() {
  }

  onSearch(search:string){
    console.log(search)
    this.watsonService.searchMockedTest().subscribe(
      data =>this.res=data
    );
  }

}
