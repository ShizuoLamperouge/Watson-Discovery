import { Component, OnInit,Input } from '@angular/core';
import {Response} from '../response';
import { nearer } from 'q';

@Component({
  selector: 'app-sentiments',
  templateUrl: './sentiments.component.html',
  styleUrls: ['./sentiments.component.css']
})
export class SentimentsComponent implements OnInit {
  @Input() response:Response;
  
  constructor() { 
  }

  ngOnInit() {
  }

}
